export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: "nuxtExample",
    htmlAttrs: {
      lang: "en",
    },
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "" },
      { name: "format-detection", content: "telephone=no" },
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ["~assets/css/reset.scss", "~assets/font/font.scss"],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    {
      src: "~/plugins/swiper.js",
      ssr: true,
    },
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: false,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: ["nuxt-gsap-module", "@nuxtjs/router"],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [],

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    extractCSS: true,
  },
  gsap: {
    extraPlugins: {
      scrollTrigger: true,
    },
  },
  router: {
    // extendRoutes(routes, resolve) {
    //   routes.push({
    //     name: "custom",
    //     path: "/page",
    //     component: resolve(__dirname, "pages/index.vue"),
    //   });
    // },
  },
};
